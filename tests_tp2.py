from tp2 import *
# on veut pouvoir créer des boîtes
def test_box_create():
	b = Box()

# on veut pouvoir mettre des trucs dedans
def test_box_add():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	
# On teste si il y a des trucs dans la boîte
def test_box_contains():
	b = Box()
	b.add("truc")
	b.add("machin")
	assert "truc" in b
	assert "bidule" not in b
	assert "machin" in b
	
def test_box_enleve():
	b = Box()
	b.add("truc")
	b.add("machin")
	b.enleve("truc")
	assert "machin" in b
	assert "truc" not in b


def test_boite_open():
	b = Box()
	# Une boîte doit être fermée par défaut
	assert not b.is_open()
	b.open()
	assert b.is_open
	b.close()
	assert not b.is_open()
	
def test_action_look():
    b = Box()
    b.add("truc")
    b.add("machin")
    b.add("bidule")
    assert b.action_look() == "La boîte est fermée !"
    b.open()
    assert b.action_look() == "La boîte contient: truc, machin, bidule"

def test_thing_create():
    chose = Thing(3)

def test_thing_volume():
    chose = Thing(5)
    assert chose.volume() == 5


def test_box_capacity():
    b = Box()
    b.set_capacity(18)
    assert b.capacity() == 18
    

# def test_has_room_for():
	
